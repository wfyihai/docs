# docs

#### 介绍
OpenBlock 手册 和 内置API

[https://openblock.gitee.io/docs/](https://openblock.gitee.io/docs/)

#### 软件架构
文档全部内容基于md完成，浏览器中可以通过index.html检索md内容。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

