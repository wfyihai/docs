# OpenBlock 使用手册

&ensp;&ensp;&ensp;&ensp;[OpenBlock](https://gitee.com/openblock/openblock)是一种专为没有技术背景的非研发人员设计的图形化脚本语言。

![界面展示](./img/功能展示.png '功能展示.png')
![界面展示](./img/功能展示1.png '功能展示1.png')

&ensp;&ensp;&ensp;&ensp;[OpenBlock](https://gitee.com/openblock/openblock)借鉴少儿编程语言 Scratch 的图形化设计，以完全图形化的方式展现逻辑，并提供大量的图形方式展示逻辑内容，好学、好理解。

[样例列表](/testing/)

在线编辑器地址：
[https://openblock.gitee.io/](https://openblock.gitee.io/)

[入门教程](./1_快速上手/1_快速上手.md)

[学习教程](./2_教程/index.md)

[模块说明](/frontpage/block-docs/index.md)

[集成手册](./5_integrate.md)

[完整目录](fullindex.md)
